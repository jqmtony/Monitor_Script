#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
if [[ -n $1 ]];then
  export ORACLE_SID=$1
fi
#env
spoolfile=$scriptsdir/tmp/ora.db.archsw.$$.out
lockurl=${scriptsdir}/locks/ora.db.archsw.lck
if [[ `islock $lockurl` -eq 1 ]];then
    #reclog "ORADB.ARCHSW" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
sqlplus "/ as sysdba">/dev/null<<EOF
spool $spoolfile
----calltime<3*8
select 'DATA,'||t.seq||','||trunc(t.fre_pers,0)||','||t.ftime||','||t.size_Kb msg from (
select  SEQUENCE#  seq,(next_time-first_time )*24*3600 fre_pers,to_char(first_time,'yyyymmddhh24:mi:ss') ftime ,
trunc(blocks*block_size/1024,0) size_Kb from v\$archived_log order by SEQUENCE# desc) t where rownum<9
;
spool off
quit
EOF
##format sqlfile
grep DATA $spoolfile|grep -v select|awk -F ',' '{print $2,$3,$4,$5}'|
while read f2 f3 f4 f5
do
  if [[ $dataformat -eq 1 ]];then
      echo "insert into mn_oradb_archsw values('$rid.db.{$ORACLE_SID}','$ORACLE_SID',$f2,$f3,$f5,to_date('$f4','yyyymmddhh24:mi:ss'),'$(date "+%Y%m%d:%H%M%S")',sysdate)">>$dataformaturl
      echo "--NEXT">>$dataformaturl
  fi
done
######Exit############################################
rm -f $spoolfile>/dev/null
unlock $lockurl
exit 0