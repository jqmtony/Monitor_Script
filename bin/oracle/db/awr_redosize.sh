#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
if [[ -n $1 ]];then
  export ORACLE_SID=$1
fi
#env
spoolfile=$scriptsdir/tmp/ora.db.redosize.$$.out
lockurl=${scriptsdir}/locks/ora.db.redosize.lck
if [[ `islock $lockurl` -eq 1 ]];then
    #reclog "ORADB.REDOSIZE" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
sqlplus "/ as sysdba">/dev/null<<EOF
set embedded off
ttitle off
btitle off
set pagesize 0
set wrap off
set linesize 2000
set trimspool on
set newpage 0
set space 0
set trunc off
set arraysize 1
set heading off
set feedback off
set verify off
set termout off
set underline '-'
set pause off
clear breaks
clear columns
col message for a90
spool $spoolfile
select 'DATA,'||key_time||','||value||',' from (
    select   t.key_time,trunc(t.redosize_pers/1024,2) value
      from (select sn.instance_number,
                   to_char(sn.end_interval_time, 'yyyymmdd-hh24:mi:ss') key_time,
                   sn.snap_id "snap_id",
                   (ns.AA - os.AA) /
                   ((to_date(to_char(sn.end_interval_time,
                                     'yyyymmddhh24miss'),
                             'yyyymmddhh24miss') -
                   to_date(to_char(so.end_interval_time,
                                     'yyyymmddhh24miss'),
                             'yyyymmddhh24miss')) * 86400) redosize_pers
              from (select instance_number, snap_id, sum(value) as "AA"
                      from Wrh\$_Sysstat
                     where stat_id in
                           (select stat_id
                              from wrh\$_stat_name
                             where stat_name in ('redo size'))
                     group by instance_number, snap_id) ns,
                   (select instance_number, snap_id, sum(value) as "AA"
                      from Wrh\$_Sysstat
                     where stat_id in
                           (select stat_id
                              from wrh\$_stat_name
                             where stat_name in ('redo size'))
                     group by instance_number, snap_id) os,
                   wrm\$_snapshot sn,
                   wrm\$_snapshot so
             where ns.snap_id = os.snap_id + 1
               and ns.instance_number = os.instance_number
               and ns.snap_id = sn.snap_id
               and ns.instance_number = sn.instance_number
               and sn.snap_id = so.snap_id + 1
               and sn.instance_number = so.instance_number) t) where to_date(key_time,'yyyymmdd-hh24:mi:ss')>sysdate-1/24
     ORDER BY 1 desc;
 
spool off
quit
EOF
##format sqlfile
grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $2,$3}'|
while read f2 f3
do
if [[ $dataformat -eq 1 ]];then
	#echo "insert into mn_oradb_redosize values('$rid.db.{$ORACLE_SID}','$ORACLE_SID',to_date('$f2','yyyymmdd-hh24:mi:ss'),$f3,to_date('$f2','yyyymmdd-hh24:mi:ss'))"
    echo "insert into mn_oradb_redosize values('$rid.db.{$ORACLE_SID}','$ORACLE_SID',to_date('$f2','yyyymmdd-hh24:mi:ss'),$f3,to_date('$f2','yyyymmdd-hh24:mi:ss'))">>$dataformaturl
    echo "--NEXT">>$dataformaturl
fi
done

######Exit############################################
rm -f $spoolfile>/dev/null
unlock $lockurl
exit 0