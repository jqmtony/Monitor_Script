#!/bin/sh
# It will monitor archivelog destination occupation.
# It had run on oracle 10g,on standby 
# crontab:
#	5 5 * * *  sh /mn/arch.sh>/tmp/dg.log
#################################### function start ####################################
rlog()
{
	RQ=`date +'%Y%m%d-%H:%M:%S'` 
	echo "$RQ | $1 | $2">>$LOGFILE
}
#################################### function end ######################################
#################################### configuration start ############################### 
# The Base Directory
BASEDIR=/mn
LOGFILE=${BASEDIR}/arch.log

##################################
# ORACLE SID
. /home/oracle/.profile
ORACLESID=BI

##################################
# FUN_ARCH:true|false
# THREAD:1|2
# ArchKeep:" " | " and  next_time<sysdate-5 "  
#
FUN_ARCH=true
THREAD=1
#noram 
SEQSQL="select thread#,max(sequence#) from v\$archived_log where ARCHIVED='YES' and  next_time<sysdate-5 group by thread#;"  
#standby  
#SEQSQL="select thread#,max(sequence#) from v\$archived_log where ARCHIVED='YES' and APPLIED='YES' and  next_time<sysdate-7 group by thread#;"  
#primary 
#SEQSQL="select thread#,max(sequence#) from v\$archived_log where ARCHIVED='YES' and APPLIED='YES' group by thread#;"  

##################################
# FUN_FS:true|false
#FUN_FS=true
#FS_LOW_PERCENT=30
#FS_HIGH_PERCENT=50
#LOG_DEST1=/kfarch1
#################################### configuration end #################################
#################################### Main Start ########################################
export ORACLE_SID=$ORACLESID
rlog "JOB START" "#################################"
# FUN_ARCH MAIN
rlog "FUN_ARCH" "Arch is enabled,THREAD=${THREAD}."
if [[ $FUN_ARCH = "true" ]];then
	RMANSCRIPT=${BASEDIR}/rman_arch.cmd
	RMANRUNLOG=${BASEDIR}/rman_run.log
	>$RMANSCRIPT
	THREAD1_WAIT_SEQ=1
	THREAD2_WAIT_SEQ=1
	THREAD1_DELETED_SEQ=1
	THREAD2_DELETED_SEQ=1
	ARCHFLAG=0
	################get sequence
	tempfile=${BASEDIR}/seq.tmp
	>$tempfile
	sqlplus "/ as sysdba"   1>>/dev/null 2>>/dev/null <<SQL_SCRIPT
	set termout off heading off echo off time off timing off feedback off
	spool $tempfile
    $SEQSQL
	spool off
	exit
SQL_SCRIPT
	if [[ $THREAD -eq 1 ]];then
		THREAD1_WAIT_SEQ=`grep " 1 " $tempfile| tail -n 1 |awk '{print $2}' - `
		THREAD1_DELETED_SEQ=`expr $THREAD1_WAIT_SEQ - 10 `
		rlog "FUN_ARCH" "THREAD1_WAIT_SEQ:$THREAD1_WAIT_SEQ,THREAD1_DELETED_SEQ:$THREAD1_DELETED_SEQ"
		if [[ -n $THREAD1_DELETED_SEQ && $THREAD1_DELETED_SEQ  -gt 10 ]];then
			ARCHFLAG=1
			rlog "FUN_ARCH" "THREAD1_DELETED_SEQ Check OK."
		else
			rlog "FUN_ARCH" "THREAD1_DELETED_SEQ Check ERROR."
		fi
	elif [[ $THREAD -eq 2 ]];then
		THREAD1_WAIT_SEQ=`grep " 1 " $tempfile| tail -n 1 |awk '{print $2}' - `	
		THREAD2_WAIT_SEQ=`grep " 2 " $tempfile| tail -n 1 |awk '{print $2}' - `
		THREAD1_DELETED_SEQ=`expr $THREAD1_WAIT_SEQ - 15 `
		THREAD2_DELETED_SEQ=`expr $THREAD2_WAIT_SEQ - 15 `
		rlog "FUN_ARCH" "THREAD1_WAIT_SEQ:$THREAD1_WAIT_SEQ,THREAD1_DELETED_SEQ:$THREAD1_DELETED_SEQ"
		rlog "FUN_ARCH" "THREAD2_WAIT_SEQ:$THREAD2_WAIT_SEQ,THREAD2_DELETED_SEQ:$THREAD2_DELETED_SEQ"
		if [[ -n $THREAD1_DELETED_SEQ &&  -n $THREAD2_DELETED_SEQ && $THREAD1_DELETED_SEQ -gt 15 && $THREAD2_DELETED_SEQ -gt 15 ]];then
			ARCHFLAG=1
			rlog "FUN_ARCH" "THREAD1_DELETED_SEQ Check OK."
		else
			rlog "FUN_ARCH" "THREAD1_DELETED_SEQ Check ERROR."
		fi
	fi
	################get rman scripts
	if [[ $ARCHFLAG -eq 1 ]];then
		echo " run {                                                      "                    >>$RMANSCRIPT
		echo " allocate channel d1 type disk;                             "                    >>$RMANSCRIPT
		echo " crosscheck archivelog all;                                 "                    >>$RMANSCRIPT
		echo " delete expired archivelog all;							  "				       >>$RMANSCRIPT
		echo " delete noprompt archivelog until sequence $THREAD1_DELETED_SEQ thread 1;    "   >>$RMANSCRIPT
		if [[ $THREAD -eq 2 ]];then
			echo " delete noprompt archivelog until sequence $THREAD2_DELETED_SEQ thread 2; "  >>$RMANSCRIPT
		fi
		echo " release channel d1;                                        "                    >>$RMANSCRIPT
		echo " }                                                          "                    >>$RMANSCRIPT	
	fi
	#cat $RMANSCRIPT
	################rman run
	rlog "FUN_ARCH" "Rman delete arch run."
	rman target / NOCATALOG \@$RMANSCRIPT log="$RMANRUNLOG"
fi
# FUN_FS MAIN
# NO 
#################################### Main End ########################################
rlog "JOB FINISHED" "#################################"
exit 0
 