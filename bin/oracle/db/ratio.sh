#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
if [[ -n $1 ]];then
  export ORACLE_SID=$1
fi
spoolfile=$scriptsdir/tmp/ora.db.ratio.$$.out
lockurl=${scriptsdir}/locks/ora.db.ratio.lck
if [[ `islock $lockurl` -eq 1 ]];then
    #reclog "ORADB.RATIOS" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
sqlplus "/ as sysdba">/dev/null<<EOF
set embedded off
ttitle off
btitle off
set pagesize 0
set wrap off
set linesize 2000
set trimspool on
set newpage 0
set space 0
set trunc off
set arraysize 1
set heading off
set feedback off
set verify off
set termout off
set underline '-'
set pause off
clear breaks
clear columns
col msg for a90
spool $spoolfile
select 'DATA' || ',' || t.name || ',' || trunc(t.value, 2) message
  from (select 'buffer_hit' name,
               round((1 - (c.value / (a.value + b.value))) * 100, 3) value
          from v\$sysstat a, v\$sysstat b, v\$sysstat c
         where a.name = 'db block gets'
           and b.name = 'consistent gets'
           and c.name = 'physical reads'
        union all
        select 'memory_sort_ratio' name,
               round(a.value / (a.value + b.value) * 100, 2) value
          from v\$sysstat a, v\$sysstat b
         where a.name = 'sorts (memory)'
           and b.name = 'sorts (disk)'
        union all
        select 'library_hit' name,
               round((1 - (sum(reloads) / sum(pins))) * 100, 3) value
          from v\$librarycache
        union all
        select 'execute_to_parse' name,
               decode(t.e, 0, -1, trunc((1 - (t.p / t.e)) * 100, 3)) value
          from (select (select value
                          from v\$sysstat
                         where name = 'parse count (total)') p,
                       (select value
                          from v\$sysstat
                         where name = 'execute count') e
                  from dual) t
        union all
        select 'parse_cpu_to_parse_elapsd' name,
               decode(t.e, 0, -1, trunc((1 - (t.c / t.e)) * 100, 3)) value
          from (select (select value
                          from v\$sysstat
                         where name = 'parse time cpu') c,
                       (select value
                          from v\$sysstat
                         where name = 'parse time elapsed') e
                  from dual) t
        union all
        select 'dictionary_hit' name,
               round((1 - sum(getmisses) / sum(gets)) * 100, 3) value
          from v\$rowcache
        union all
        select 'latch_hit' name,
               round((select sum(gets) - sum(misses) from v\$latch) /
                     (select sum(gets) from v\$latch) * 100,
                     3) value
          from dual
        union all
        select 'rollback_hit' name,
               decode(t.g, 0, -1, trunc((1 - (t.w / t.g)) * 100, 3)) value
          from (select sum(waits) w, sum(gets) g from v\$rollstat) t) t;
spool off
quit
EOF
##format sqlfile
grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $2,$3}'|
while read f2 f3
do
if [[ $dataformat -eq 1 ]];then
    echo "insert into mn_oradb_ratio values('$rid.db.{$ORACLE_SID}','$ORACLE_SID','$f2',$f3,'$(date "+%Y%m%d:%H%M%S")',sysdate)">>$dataformaturl
    echo "--NEXT">>$dataformaturl
fi
done
######Exit############################################
rm -f $spoolfile>/dev/null
unlock $lockurl
exit 0