#!/bin/sh
scripts_dir=$1
sid=$2
PROG_HOME=${scripts_dir}/tmp/data
if [[ ! -d $PROG_HOME ]];then
	mkdir -p $PROG_HOME
fi

if [[ ! -n $PROG_HOME ]];then
	exit 1
fi
#environment
ostype=`uname 2>/dev/null`
time=`date +"%Y%m%d%H%M"`
case $ostype in
  linux*|LINUX*|Linux*)
    . $HOME/.bash_profile
        ;;
        AIX*|aix*)
    . $HOME/.profile
        ;;
        HP*|hp*)
        . $HOME/.profile
        ;;
  *)
   . $HOME/.profile
    ;;
esac
ORACLE_SID=$sid;export ORACLE_SID
stty -istrip cs8

#check before
count=`ps -ef |grep $0 |grep -v grep |wc -l`
if [[ $count -gt 3 ]]; then
        echo $0 already running!
        exit 1
fi 


$ORACLE_HOME/bin/sqlplus -s /nolog<<EOF
connect / as sysdba;
set head off
set feedback off
spool $PROG_HOME/test.log
select distinct sl.sql_hash_value from v\$session_longops sl,v\$session se  
where sl.sid=se.sid and sl.opname not like 'RMAN%' 
and  sl.time_remaining>0 and sl.totalwork*8/1024>1000 
and (SYSDATE-sl.start_time)*24*60<10 ;
spool off
--insert into perfstat.tablescan_log1 select 'CRMDB02',sl.sid,sl.serial#,se.username,se.osuser,machine,program,sl.sql_id,sl.SQL_HASH_VALUE
--,sysdate from v\$session_longops sl,v\$session se  where sl.sid=se.sid and sl.opname not like 'RMAN%' and  sl.time_remaining>0 and sl.to
--talwork*8/1024>1000 and (SYSDATE-sl.start_time)*24*60<10 and se.machine in('bss2vac','crm1app','crm2app','prmdb','prmdb_per','test','wai
--hu01','waihu02','waihu03','waihu04','web1','web10','web2','web3','web4','web5','web6','web7','web8','web9','zwdb01');
--INSERT INTO tablescan_log@CRM_DBLINK_ORAMON1  select * from perfstat.tablescan_log1;
--delete from perfstat.tablescan_log1;
commit;
exit
EOF

cat $PROG_HOME/test.log|awk '{print $1}'>$PROG_HOME/test1.log
sed '1d' $PROG_HOME/test1.log>$PROG_HOME/test.log
cat $PROG_HOME/test.log

cat $PROG_HOME/test.log|awk '{print $1}'|while read file_hash_value
do
$ORACLE_HOME/bin/sqlplus -s /nolog<<EOF
connect / as sysdba;
set head off 
set linesize 141
spool $PROG_HOME/${ORACLE_SID}.table_scan$file_hash_value.${time}.log

col machine format a20
col username format a13
col target format a30
col exe_times format a5

select to_char(sysdate,'YYYY-MM-DD HH24:MI')||': Table Scan found at CRMDB Hash_value:$file_hash_value' from dual; 
set head on
--select to_char(first_time,'YYYY-MM-DD HH24:MI') first_time,to_char(last_time,'YYYY-MM-DD HH24:MI') last_time,total,last_7_day,today,
--status from v\$scan where sql_hash_value='$file_hash_value';

select username, target, sofar*8/1024 do_now,totalwork*8/1024 total_work_MB, sql_hash_value,to_char(start_time,'hh24:mi:ss') starttime, 
time_remaining+elapsed_seconds run_times from  v\$session_longops where sql_hash_value='$file_hash_value' and totalwork*8/1024>1000 and 
(SYSDATE-start_time)*24*60<10;

set linesize 80
col first_load_time format a25
select disk_reads, buffer_gets, executions,first_load_time first_load_time from v\$sqlarea where hash_value='$file_hash_value';
col osuser format a20
col program format a40
col process format a10
col osuser format a10 
set linesize 121
select sid,serial#,osuser,process,machine,program from v\$session  
where sid in (select sid from v\$session_longops where sql_hash_value='$file_hash_value' and totalwork*8/1024>100 
and (SYSDATE-start_time)*24*60<10);

set head off
set pages 999
select sql_text from v\$sqltext where hash_value='$file_hash_value' order by piece;
--exec perfstat.create_sql($file_hash_value);
set heading off
select '--------------------------------------------------------------------------------' from dual
union all
select '| Operation                                                    | PHV/Object Name     |  Rows | Bytes| Cost |' as "Optimizer Plan
:" from dual
union all
select '--------------------------------------------------------------------------------' from dual
union all
select *
from (select
rpad('|'||substr(lpad(' ',1*(depth-1))||operation||
decode(options, null,'',' '||options), 1, 62), 63, ' ')||'|'||
rpad(decode(id, 0, '----- '||to_char(hash_value)||' -----'
, substr(decode(substr(object_name, 1, 7), 'SYS_LE_', null, object_name)
||' ',1, 20)), 21, ' ')||'|'||
lpad(decode(cardinality,null,' ',
decode(sign(cardinality-10000), -1, cardinality||' ', 
decode(sign(cardinality-1000000), -1, trunc(cardinality/1000)||'K', 
decode(sign(cardinality-1000000000), -1, trunc(cardinality/1000000)||'M', 
trunc(cardinality/1000000000)||'G')))), 7, ' ') || '|' ||
lpad(decode(bytes,null,' ',
decode(sign(bytes-1024), -1, bytes||' ', 
decode(sign(bytes-1048576), -1, trunc(bytes/1024)||'K', 
decode(sign(bytes-1073741824), -1, trunc(bytes/1048576)||'M', 
trunc(bytes/1073741824)||'G')))), 6, ' ') || '|' ||
lpad(decode(cost,null,' ',
decode(sign(cost-10000000), -1, cost||' ', 
decode(sign(cost-1000000000), -1, trunc(cost/1000000)||'M', 
trunc(cost/1000000000)||'G'))), 8, ' ') || '|' as "Explain plan"
from v\$sql_plan
where hash_value = $file_hash_value)
union all
select '--------------------------------------------------------------------------------' from dual;

spool off
quit
EOF
done


rm -f $PROG_HOME/result_topcpu.*.log >/dev/null
if [[ -d $PROG_HOME ]];then
	cd /tmp
	cd $PROG_HOME
	find . -mtime +7 -name "*.txt" -exec rm -f {} \;
fi


exit 0
