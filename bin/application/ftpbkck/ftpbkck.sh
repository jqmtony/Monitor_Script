#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#just run on linux
######Init############################################
bkitems='
gc|/ftp/backup/gc|77|
xxb|/ftp/backup/xxb|77|
callcenter|/ftp/backup/callcenter|77|
webvip|/ftp/backup/webvip|77|
'

scriptsdir=$(cd `dirname $0`; pwd)/../../..
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
. ${scriptsdir}/lib/sysf/os_changedate.fun
#Usage:changedate [-b|-a] [n(days)] [Now(yyyymmdd)]
######Main############################################
#/data/app/time(20151103)
#7,加上今天保留8天
#
tempfile=${scriptsdir}/tmp/app.ftpbkck.tmp
>$tempfile
today=$(date "+%Y%m%d")
yesterday=$(changedate -b 1 $today )
for bkitems in $bkitems
do
	itemname=$(echo $bkitems|awk -F '|' '{print $1}')
	itemdir=$(echo $bkitems|awk -F '|' '{print $2}')
	itemkeepday=$(echo $bkitems|awk -F '|' '{print $3}')
	#process check yesterday
	#echo $itemdir/$yesterday
	if [[ -d $itemdir/$yesterday && $(ls $itemdir/$yesterday|wc -l) -ge 1 ]];then
		itemfiles=""
		for itemfile in `ls $itemdir/$yesterday`
		do
			itemfiles="$itemfiles $itemfile"
		done
		if [[ $itemkeepday -ge 0 && $itemkeepday -le 90 ]];then
			find . -mtime +$itemkeepday  -exec rm -rf {} \;
		fi
		echo "Item:$itemname,Status=success,Keepday=$itemkeepday,Dir=$itemdir/$yesterday,File=$itemfiles,Size=$(du -sm $itemdir/$yesterday|awk '{print $1}')m">>$tempfile
	else
		echo "Item:$itemname,Status=failed,Keepday=$itemkeepday,Dir=$itemdir/$yesterday">>$tempfile
		continue;
	fi
	#porcess today
	if [[ -d $itemdir/$today && $(ls $itemdir/$today|wc -l) -ge 1 ]];then
		itemfiles=""
		for itemfile in `ls $itemdir/$today`
		do
			itemfiles="$itemfiles $itemfile"
		done
		if [[ $itemkeepday -ge 0 && $itemkeepday -le 90 ]];then
			find . -mtime +$itemkeepday  -exec rm -rf {} \;
		fi
		echo "Item:$itemname,Keepday=$itemkeepday,Dir=$itemdir/$today,File=$itemfiles,Size=$(du -sm $itemdir/$today|awk '{print $1}')m">>$tempfile
	fi
	
done

######Main############################################
#formatsqlfile
if [[ $dataformat -eq 1 ]];then
	while read LINE
		do
			flag=0
			if [[ $(echo $LINE|grep failed|wc -l) -eq 1 ]];then
				flag=3
			fi
			#echo "insert into mn_node_log values('`hostname`$rid','backup_ftp',$flag,'$LINE',' ',sysdate)" 
			echo "insert into mn_node_log values('`hostname`$rid','backup_ftp',$flag,'$LINE',' ',sysdate)">>$dataformaturl
			echo "--NEXT" >>$dataformaturl
		done < $tempfile
fi
######Exit############################################
exit 0
