#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#os_cmd.fun:VMSTAT
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../..
#include libs and pars
#. ${scriptsdir}/lib/sysf/reclog.fun
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
. ${scriptsdir}/lib/sysf/os_cmd.fun
#include Environment
vmstatfile=$scriptsdir/tmp/vmstat.$$.tmp
sarfile=$scriptsdir/tmp/sar.$$.tmp
lockurl=${scriptsdir}/locks/os.cpumem.lck
datafile=${scriptsdir}/data/os.cpumem.dat
#set lock
if [[ `islock $lockurl` -eq 1 ]];then
	#reclog "OS.CPUMEM" "ERROR:file is locked."
	exit 1
fi
lock $lockurl $$
######Main############################################
rcount=3
const=999
cusr=0;
csys=0;
cidl=0;
cwio=0;
fmem=0;
pgpo=0;
pgpi=0;
pgsr=0;
pgfr=0;
trunp=0;
tcusr=0;
tcsys=0;
tcidl=0;
tcwio=0;
tfmem=0;
tpgpo=0;
tpgpi=0;
tpgsr=0;
tpgfr=0;
runp=0;
##data
($VMSTAT 1 $rcount 2>/dev/null|
grep -v AIX|
grep -v HP|
grep -v r |
grep -v Linux|
grep -v "-")>$vmstatfile
case $ostype in
	AIX*|aix*)
		runp=`cat $vmstatfile | awk '{print $1}' |awk '{sum += $1} END {print sum}'`
		cusr=`cat $vmstatfile | awk '{print $14}'|awk '{sum += $1} END {print sum}'`
		csys=`cat $vmstatfile | awk '{print $15}'|awk '{sum += $1} END {print sum}'`
		cidl=`cat $vmstatfile | awk '{print $16}'|awk '{sum += $1} END {print sum}'`
		cwio=`cat $vmstatfile | awk '{print $17}'|awk '{sum += $1} END {print sum}'`
		fmem=`cat $vmstatfile | awk '{print $4}' |awk '{sum += $1} END {print sum}'`
		pgpo=`cat $vmstatfile | awk '{print $7}' |awk '{sum += $1} END {print sum}'`
		pgpi=`cat $vmstatfile | awk '{print $6}' |awk '{sum += $1} END {print sum}'`
		pgfr=`cat $vmstatfile | awk '{print $8}' |awk '{sum += $1} END {print sum}'`
		pgsr=`cat $vmstatfile | awk '{print $9}' |awk '{sum += $1} END {print sum}'`
	;;
	linux*|LINUX*|Linux*)
		runp=`cat $vmstatfile | awk '{print $1}' |awk '{sum += $1} END {print sum}'`
		cusr=`cat $vmstatfile | awk '{print $13}'|awk '{sum += $1} END {print sum}'`
		csys=`cat $vmstatfile | awk '{print $14}'|awk '{sum += $1} END {print sum}'`
		cidl=`cat $vmstatfile | awk '{print $15}'|awk '{sum += $1} END {print sum}'`
		fmem=`cat $vmstatfile | awk '{print $4}' |awk '{sum += $1} END {print sum}'`
		pgpo=`cat $vmstatfile | awk '{print $8}' |awk '{sum += $1} END {print sum}'`
		pgpi=`cat $vmstatfile | awk '{print $7}' |awk '{sum += $1} END {print sum}'`
		cwio=`cat $vmstatfile | awk '{print $16}'|awk '{sum += $1} END {print sum}'`
		pgfr=`expr ${rcount} \* ${const}`
		pgsr=`expr ${rcount} \* ${const}`
	;;
	HP*|hp*)
		runp=`cat $vmstatfile | awk '{print $1}' |awk '{sum += $1} END {print sum}'`
		cusr=`cat $vmstatfile | awk '{print $16}'|awk '{sum += $1} END {print sum}'`
		csys=`cat $vmstatfile | awk '{print $17}'|awk '{sum += $1} END {print sum}'`
		cidl=`cat $vmstatfile | awk '{print $18}'|awk '{sum += $1} END {print sum}'`
		fmem=`cat $vmstatfile | awk '{print $5}' |awk '{sum += $1} END {print sum}'`
		pgpo=`cat $vmstatfile | awk '{print $9}' |awk '{sum += $1} END {print sum}'`
		pgpi=`cat $vmstatfile | awk '{print $8}' |awk '{sum += $1} END {print sum}'`
		pgfr=`expr ${rcount} \* ${const}`
		pgsr=`expr ${rcount} \* ${const}`
		if [[ $rcount -lt 3 ]];then
			rcount=8;
		fi
		(sar 1 $rcount 2>/dev/null|grep -v HP|grep -v wio|grep -v hp )>$sarfile
		if [[ -f $sarfile ]];then
			cwio=`cat $sarfile|grep -v "Average"|awk '{print $4}'|awk '{sum += $1} END {print sum}'`
			cusr=`cat $sarfile|grep -v "Average"|awk '{print $2}'|awk '{sum += $1} END {print sum}'`
			csys=`cat $sarfile|grep -v "Average"|awk '{print $3}'|awk '{sum += $1} END {print sum}'`
			cidl=`cat $sarfile|grep -v "Average"|awk '{print $5}'|awk '{sum += $1} END {print sum}'`
			rm $sarfile>/dev/null 2>&1
		else
			cwio=`expr ${rcount} \* ${const}`
		fi
	;;
esac
trunp=`awk 'BEGIN{printf ("%.0f",'$trunp'+'$runp')}'`
tcusr=`awk 'BEGIN{printf ("%.0f",'$tcusr'+'$cusr')}'`
tcsys=`awk 'BEGIN{printf ("%.0f",'$tcsys'+'$csys')}'`
tcidl=`awk 'BEGIN{printf ("%.0f",'$tcidl'+'$cidl')}'`
tcwio=`awk 'BEGIN{printf ("%.0f",'$tcwio'+'$cwio')}'`
tfmem=`awk 'BEGIN{printf ("%.0f",'$tfmem'+'$fmem')}'`
tpgpo=`awk 'BEGIN{printf ("%.0f",'$tpgpo'+'$pgpo')}'`
tpgpi=`awk 'BEGIN{printf ("%.0f",'$tpgpi'+'$pgpi')}'`
tpgfr=`awk 'BEGIN{printf ("%.0f",'$tpgfr'+'$pgfr')}'`
tpgsr=`awk 'BEGIN{printf ("%.0f",'$tpgsr'+'$pgsr')}'`
trunp=`awk 'BEGIN{printf ("%.0f",'$trunp'/'$rcount')}'` 
tcusr=`awk 'BEGIN{printf ("%.0f",'$tcusr'/'$rcount')}'` 
tcsys=`awk 'BEGIN{printf ("%.0f",'$tcsys'/'$rcount')}'` 
tcidl=`awk 'BEGIN{printf ("%.0f",'$tcidl'/'$rcount')}'` 
tcwio=`awk 'BEGIN{printf ("%.0f",'$tcwio'/'$rcount')}'` 
tfmem=`awk 'BEGIN{printf ("%.0f",'$tfmem'/'$rcount')}'` 
tpgpo=`awk 'BEGIN{printf ("%.0f",'$tpgpo'/'$rcount')}'` 
tpgpi=`awk 'BEGIN{printf ("%.0f",'$tpgpi'/'$rcount')}'` 
tpgfr=`awk 'BEGIN{printf ("%.0f",'$tpgfr'/'$rcount')}'` 
tpgsr=`awk 'BEGIN{printf ("%.0f",'$tpgsr'/'$rcount')}'` 
#formatsqlfile
if [[ $dataformat -eq 1 ]];then
	echo "insert into mn_os_cpumem values('$rid','$trunp','$tcusr','$tcsys','$tcidl','$tcwio','$tfmem','$tpgpi','$tpgpo','$tpgfr','$tpgsr','$(date "+%Y%m%d:%H%M%S")',sysdate)">>$dataformaturl
	echo "--NEXT">>$dataformaturl
fi
######Exit############################################
rm -f $sarfile>/dev/null
rm -f $vmstatfile>/dev/null
unlock $lockurl
exit  0