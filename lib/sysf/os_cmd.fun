ostype=`uname 2>/dev/null`
case $ostype in
	linux*|LINUX*|Linux*)
	VMSTAT="vmstat "
	DF="df -Pkl "
	OS_TYPE=LINUX
	;;
	AIX*|aix*)
	VMSTAT="vmstat "
	DF="df -k "
	OS_TYPE=AIX
	;;
	HP*|hp*)
	VMSTAT="vmstat -S "
	DF="bdf -l "
	OS_TYPE=HP
	;;
	*)
	VMSTAT="vmstat "
	DF="df "
	OS_TYPE=UNKNOW
	;;
esac