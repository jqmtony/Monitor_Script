--统计每个对象每个小时的增长值，保留3天
delete from   mon$obj_incr_per_h  t
where t.LOG_DATE <sysdate-3;
commit;

insert into mon$obj_incr_per_h 
select a.db_name,
       a.tablespace_name,
			 a.owner,
       a.table_name,
       b.size_m,
       trunc((b.size_m - a.size_m), 2) incr_size,
       b.end_date
      -- to_char(end_date,'yyyy-mm-dd hh24:mi:ss') ,
      -- to_char(start_date,'yyyy-mm-dd hh24:mi:ss') ,
      -- a.size_m
  from (select db_name,
               tablespace_name,
							 owner,
               table_name,
               log_date start_date,
               size_m
          from mon$segment_space
         where (db_name, log_date) in
               (select db_name, max(log_date)
                  from mon$segment_space
                 where (db_name, log_date) not in
                       (select db_name, max(log_date)
                          from mon$segment_space
                         group by db_name)
                 group by db_name)) a,
       (select db_name,
               tablespace_name,
							 owner,
               table_name,
               log_date end_date,
               size_m
          from mon$segment_space
         where (db_name, log_date) in
               (select db_name, max(log_date)
                  from mon$segment_space
                 group by db_name)) b
 where a.db_name = b.db_name
   and a.tablespace_name = b.tablespace_name
   and a.table_name = b.table_name
	 and a.owner=b.owner
--  and (b.end_date - a.start_date) * 24 = 1
and b.size_m<>a.size_m
--and db_name=''
 order by 4, a.db_name, a.tablespace_name, a.table_name;
commit ;
 
 
--统计每个表空间每小时增长速度top对象，无增长则不统计,数据保持10天
delete from   mon$obj_incr_per_tbs_hour  t
where t.LOG_DATE <sysdate-10;
commit;

insert into mon$obj_incr_per_tbs_hour 
select a.db_name,a.tablespace_name,a.owner,a.table_name,a.size_m,a.incr_size,a.log_date,a.xh
from (
select t.*,
ROW_NUMBER()
       OVER (PARTITION BY t.db_name,t.tablespace_name  ORDER BY t.incr_size desc ) xh       
       from mon$obj_incr_per_h  t
where  --db_name=''  and 
incr_size!=0 and
(db_name, log_date) in (select db_name, max(log_date)
                                 from  mon$obj_incr_per_h
                                group by db_name)--条件需要精确的小时，默认为最近一个小时的采集查询。
) a
where a.xh<11;
commit;



