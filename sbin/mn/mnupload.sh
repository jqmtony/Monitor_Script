#!/bin/sh
#1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,51,53,55,57,59  * * * * sh /mn/sbin/mn/mnupload.sh >/dev/null
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#env
lockurl=${scriptsdir}/locks/os.ftpupload.lck
datafile=${scriptsdir}/data/mn.ftpupload.dat
sqlurl=$agentftpupload
tmpurl=${scriptsdir}/tmp/ftpupload/dosql
if [[ ! -d $sqlurl ]];then
	exit 1
fi
if [[ ! -d $tmpurl ]];then
	mkdir -p $tmpurl 
fi
if [ `islock $lockurl` -eq 1 ];then
	echo "ERROR,MN.FTPUPLOAD file is locked."
	exit 1
fi
lock $lockurl $$
###################main
if [[ `ls -rtl $sqlurl|grep data|grep sql|wc -l` -ge 1 ]];then
	mv $sqlurl/*.data.$(date "+%Y%m%d")*.sql $tmpurl/
	cat $tmpurl/*.sql >$tmpurl/do.sql
	if [[ `cat $tmpurl/do.sql|wc -l` -ge 2 ]];then
		echo "start to insert data."
		runrst=`java -jar $scriptsdir/lib/jlibf/sqlrun.jar rsql url=jdbc:oracle:thin:@$ip:$port:$sid user=$user secpwd=$secpwd sqlfile=$tmpurl/do.sql 2>&1|grep SQLException|wc -l`
		rm -f $tmpurl/*.sql
	else
		echo "No data."
	fi
else
	echo "No sqlfile."
fi
unlock $lockurl
exit 0
