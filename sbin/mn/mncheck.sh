#!/bin/sh
#mn.cnf:user,pwd
#5 6 * * *             sh  /oracle/mn/sbin/mn/mncheck.sh soa>/dev/null
scriptsdir=$(cd `dirname $0`; pwd)/../..
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/ora_env.fun
. ${scriptsdir}/lib/sysf/reclog.fun

if [[ -n $1 ]];then
	export ORACLE_SID=$1
else
	echo "No SID"
	exit 1
fi
reclog 'MN Service' ' Mn check run.'
 

#process mn_node_heartbeat and table analyze
sqlplus $user/$pwd <<EOF
insert into mn_node_log select 'MN','MN',3,'The node '||node||' is not configure,Time:'||to_char(sysdate,'yyyy-mm-dd hh24:mi'),'msg from mn check.',sysdate from mn_node_hb_view where node not in (select node from mn_node_info);
commit;
quit
EOF

