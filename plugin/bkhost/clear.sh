#!/bin/sh
#10 10 * * * sh /mon/clear.sh >/dev/null
#xxb
#callcenter
#webvip
#####################
clients='xxb#/ftp/backup/xxb#
callcenter#/ftp/backup/callcenter#
webvip#/ftp/backup/webvip#
'
for client in $clients
do
name=`echo $client|awk -F '#' '{print $1}'`
dir=`echo $client|awk -F '#' '{print $2}'`
if [[ -d $dir ]];then
    echo "start to delete ${name}."
    cd $dir
    find . -mtime +2 -type d -exec rm -rf {} \;
fi
done
exit 0